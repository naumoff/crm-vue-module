import DashboardMailer from "./views/Dashboard";

export default [
  {
    path: "/mailer",
    name: "dashboardMailer",
    component: DashboardMailer
  }
];
