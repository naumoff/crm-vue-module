const state = {
  domain: process.env.VUE_APP_DOMAIN,
  root_api: process.env.VUE_APP_ROOT_API,
  client_id: process.env.VUE_APP_CLIENT_ID,
  client_secret: process.env.VUE_APP_SECRET,
  access_token: null
};

const getters = {
  accessToken: state => {
    return state.access_token;
  }
};

const mutations = {
  setAccessToken: (state, newValue) => {
    state.access_token = newValue;
  }
};

const actions = {};

export default {
  state,
  getters,
  mutations,
  actions
};
