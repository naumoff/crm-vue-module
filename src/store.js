import Vue from "vue";
import Vuex from "vuex";
import core from "./modules/core/core.store";
import auth from "./modules/auth/auth.store";
import crm from "./modules/crm/crm.store";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  getters: {},
  mutations: {},
  actions: {},
  modules: {
    core,
    auth,
    crm
  }
});
