import auth from "./../auth/auth.store";

const state = {
  companies: []
};

const getters = {
  getCompanies() {
    return state.companies;
  }
};

const mutations = {
  setCompanies: (state, companies) => {
    state.companies = companies;
  }
};

const actions = {
  fetchCompanies: ({commit}) => {
    console.log(auth.state.root_api);
    axios
      .get(auth.state.root_api + 'companies')
      .then(response => {
        commit('setCompanies', response.data);
      })
      .catch(error => {
        console.log(error);
      });
  }
};

export default {
  state,
  getters,
  mutations,
  actions
};
