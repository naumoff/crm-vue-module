import Vue from "vue";
import Router from "vue-router";
import authRoutes from "./modules/auth/auth.router";
import crmRoutes from "./modules/crm/crm.router";
import mailerRoutes from "./modules/mailer/mailer.router";

Vue.use(Router);

const baseRoutes = [];

let routes = baseRoutes.concat(authRoutes).concat(crmRoutes).concat(mailerRoutes);

export default new Router({
  mode: "history",
  routes
});
