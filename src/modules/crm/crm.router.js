import DashboardCRM from "./views/Dashboard";
import CompaniesList from "./views/companies/List";
import Company from "./views/companies/Company";

export default [
  {
    path: "/",
    name: "home",
    component: DashboardCRM
  },
  {
    path: "/companies",
    component: CompaniesList,
    name: "companiesList"
  },
  {
    path: "/company",
    component: Company,
    name: "company"
  }
];
