const state = {
  sidebarCollapsed: false,
  currentSidebar: 'crm',
};

const getters = {
  currentSidebar: state => {
    return state.currentSidebar;
  },

  sidebarCollapsed: state => {
    return state.sidebarCollapsed;
  },

  sidebarWidth: state => {
    if (state.sidebarCollapsed) {
      return '65px';
    } else {
      return '200px';
    }
  }
};

const mutations = {
  setSidebar: (state, sidebarName) => {
    state.currentSidebar = sidebarName;
  },
  setSidebarState: (state, isCollapsed) => {
    state.sidebarCollapsed = isCollapsed;
  }
};

const actions = {};

export default {
  state,
  getters,
  mutations,
  actions
};
